// console.log(window.location.search)

let params = new URLSearchParams(window.location.search)

let courseId = params.get("courseId")

// console.log(courseId)

let token = localStorage.getItem("token")

fetch(`http://localhost:4000/deleteCourse/${courseId}`, {
	method: 'DELETE',
	headers: {
		Authorization: `Bearer ${token}`
	}
})
.then (res => res.json())
.then (data => {
	if(data === true)
		window.location.replace("./courses.html")
})





/*
	ACTIVITY:

	Create a DELETE request via fetch that sends the courseId in the URL, then if the server responds with a true statement 
	(meaning disabling the course worked),
	immediately redirect the user to courses.html.


	Else, display an error message.

	Guide question: Who is supposed to be able to disable course?



*/